#!/usr/bin/env python3
'''Tenkey emulator.

Up arrow is Clear All.

q, C-c, C-d and C-z quit.

To accommodate the Kensington external keypad, 1 press of BS is a Clear,
and 2 presses is a Clear All.

'''
import argparse
import logging
import sys
import curses
import decimal

minver = 3,6
args = None
plus = None
minus = None
times = None
div = None
ops = None

def parse_args(argv):
    '''Parse arguments, set up logging.'''
    global args

    parser = argparse.ArgumentParser(
        description='')

    verbose_help = '-hh' in argv

    # See argparse.FileType

    parser.add_argument('-hh', action='help',
                        help="show verbose help and exit")
    parser.add_argument('-hm',
                        action='store_true',
                        required=False,
                        help=('Module help' if verbose_help
                              else argparse.SUPPRESS))
    loglevels = list(logging._levelToName.values())
    loglevels.remove('NOTSET')
    loglevels = list(map(str.lower, loglevels))
    parser.add_argument('--debug', '-d',
                        action='store_true',
                        help='Interactive debugging.')
    parser.add_argument('--log', '-l',
                        required=False,
                        metavar='LEVEL',
                        choices=loglevels,
                        help=('set logging level: ' + str(loglevels)
                              + ' [%(default)s]' if verbose_help
                              else argparse.SUPPRESS),
                        default='warning')
    parser.add_argument('--log-file', '-lf',
                        required=False,
                        help=('Log file name, '
                              'in addition to stderr'
                              if verbose_help
                              else argparse.SUPPRESS),
                        metavar='PATH')

    args = parser.parse_args(args=argv)
    level = getattr(logging, args.log.upper())
    handlers = [logging.StreamHandler(sys.stderr)]
    if args.log_file is not None:
        handlers.append(logging.FileHandler(args.log_file))
    logging.basicConfig(level=level,
                        handlers=handlers)
    if args.hm:
        help(__name__)
        sys.exit(0)

def checkminver():
    '''Insure the needed python version is used.

If an insufficient version is used, exit with code 1.'''
    if sys.version_info < minver:
        logging.critical(
            'Minimum python version is '+'.'.join(map(str,minver)))
        logging.critical(
            'Using '+'.'.join(map(str,sys.version_info)))
        sys.exit(1)


D = decimal.Decimal
class handler():
    zero = D(0)
    one = D(1)

    def __init__(self):
        self.places = 2
        self.subtotal = D(0)
        self.total = D(0)
        self.repeat = False
        self.reset = False
        self.ex = D(1)
        self.tape = []
        self.fmt = '{:15,.'+str(self.places)+'f}'
        self.point = 2

    def setReset(self):
        self.reset = True

    def doReset(self, sub=True):
        if self.reset:
            self.repeat = False
            self.subtotal = D(0)
            self.total = D(0)
            self.reset = False

    def clear(self):
        self.subtotal = D(0)
        self.ex = handler.one
        self.repeat = False

    def clearAll(self):
        self.clear()
        self.total = D(0)
        self.reset = False
        self.tape.append('')
        self.tape.append('CA')
        self.tape.append('')

    def numeric(self, key):
        ###
        digits = [ord(x) for x in '0123456789']
        self.doReset()

        if self.repeat:
            self.repeat = False
            self.subtotal = D(0)
        if key in digits:
            if self.ex == D(1):
                self.subtotal *= 10
                self.subtotal += key - ord('0')
            else:
                self.subtotal += D((key - ord('0'))) * self.ex
                self.ex /= D(10)
        elif key == ord('.'):
            self.ex = 1 / D(10)

    def op(self, key):
        self.subtotal = self.subtotal.quantize(
            D(10) ** -self.places)
        if key in plus:
            if self.ex == D(1) and not self.repeat:
                self.subtotal = self.subtotal / (D(10) ** self.point)
            self.total += self.subtotal
            sign = ' +'
        elif key in minus and not self.repeat:
            if self.ex == D(1):
                self.subtotal = self.subtotal / (D(10) ** self.point)
            self.total -= self.subtotal
            sign = ' -'
        self.tape.append(self.fmt.format(self.subtotal) + sign)
        self.ex = D(1)
        self.repeat = True

    def doTotal(self):
        self.tape.append(self.fmt.format(self.total) + ' *')
        self.tape.append('')
        self.tape.append('')
        self.subtotal = self.total
        self.total = D(0)
        self.reset = False


def tk(stdscr):
    h = handler()
    k = 0
    clearcount = 0
    curses.mousemask(True)
    stdscr.clear()
    stdscr.refresh()

    digits = [ord(x) for x in '0123456789']
    numeric = digits.copy()
    numeric.append(ord('.'))

    while True:
        stdscr.clear()
        height, width = stdscr.getmaxyx()


        if k in [curses.KEY_RIGHT, 127]:
            clearcount += 1
            if clearcount == 1:
                h.clear()
            elif clearcount == 2:
                h.clearAll()
        else:
            clearcount = 0

        if k in numeric:
            h.numeric(k)

        elif k in ops:
            h.op(k)

        elif k == 10 or k == curses.PADENTER:
            # PADENTER for Windows
            # 10 for others
            h.doTotal()

        elif k == curses.KEY_UP:
            h.clearAll()

        elif k in [ord('q'), 3, 4, 26]:
            break

        elif k == curses.KEY_MOUSE:
            t = curses.getmouse()
            if args.debug:
                _, mx, my, _, _ = t
                stdscr.addstr(5, 0, str(t))

        totalstr = str(h.fmt.format(h.total))
        stdscr.addstr(0, width-len(totalstr), totalstr)
        subtotalstr = str(h.fmt.format(h.subtotal))
        stdscr.addstr(1, width-len(subtotalstr), subtotalstr)
        if args.debug:
            stdscr.addstr(2, 0, '{} {}'.format(k, curses.keyname(k)))
            stdscr.addstr(3, 0, str(h.ex))
            stdscr.addstr(4, 0, str(clearcount))

        if len(h.tape) > 0:
            for n, r in enumerate(range(height-1, 4, -1)):
                if n >= len(h.tape):
                    break;
                t = h.tape[-n-1]
                stdscr.addstr(r, width-len(t)-1, t)

        stdscr.refresh()
        k = stdscr.getch()

def main(argv=None):
    global plus, minus, times, div, ops

    if argv is None:
        argv = sys.argv[1:]

    parse_args(argv)
    checkminver()
    if 'PADMINUS' not in curses.__dict__:
        # Add objects from windows-curses if missing
        curses.PADPLUS = ord('+')
        curses.PADMINUS = ord('-')
        curses.PADSTAR = ord('*')
        curses.PADSLASH = ord('/')
        curses.PADENTER = 10
    plus = [ord('+'), curses.PADPLUS]
    minus = [ord('-'), curses.PADMINUS]
    times = [ord('*'), curses.PADSTAR]
    div = [ord('/'), curses.PADSLASH]
    ops = plus + minus + times + div

    curses.wrapper(tk)

if __name__ == "__main__":
    sys.exit(main())
